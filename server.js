import express from 'express';
import bodyParser from 'body-parser';
import cookieSession from 'cookie-session';
import passport from 'passport';
import logger from './core/logger/app-logger'
import morgan from 'morgan'
import config from './core/config/config.dev'
import connectToDb from './db/connect';
import path from 'path';
import socketIo from './services/socketIo';
import Passport from './services/passport';
import combineRoutes from './routes/combineRoutes';
import cors from 'cors';
import {corsOptionsDelegate} from './services/cors.service';

const jwtSecrect = "lasfu";
//passport setup

const port = config.serverPort;
logger.stream = {
    write: function (message, encoding) {
        // console.log(encoding);
        logger.info(message);
    }
};
connectToDb();
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(morgan('dev', {'stream': logger.stream}));
app.use(cookieSession({
    //       d    hh    mm  ss
    maxAge: 1 * 24 * 60 * 60 * 1000,
    keys: jwtSecrect
}))
// app.use(cookieParser(jwtSecrect)); app.use(session({     secret: jwtSecrect,
//    resave: false,     saveUninitialized: true,     cookie: { maxAge: 60000
// }})); app.use(flash());
Passport();
app.use(passport.initialize());
app.use(passport.session());
app.use(cors(corsOptionsDelegate));
app.options('*', cors(corsOptionsDelegate));

combineRoutes(app);

//Index route
app.use(express.static(path.resolve(__dirname, 'client', 'dist')));

app.get('*', (req, res) => {

    res.sendFile(path.resolve(__dirname, 'client', 'dist', 'index.html'));
});

const server = socketIo(app);

server.listen(port, () => {
    logger.info('server started - ', port);
});