import express from 'express';
import controller from '../controllers/user.controller';
import ResourcesRoutes from './Routes/resourcesRoutes';
import authorize from '../services/authorize';
let router = express.Router()

router = ResourcesRoutes(router, controller);


router.post('/localAdd',authorize.authorizedClientSecret ,(req,res)=>{
    controller.localAdd(req, res);
})

router.get('/test', (req, res) => {
    controller.getTest(req, res);
})
/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});

/* GET user profile. */
router.get('/profile', function (req, res, next) {
    res.send(req.user);
});

router.get('/setup', function (req, res) {
    controller.setUp(req, res);
});
router.get('/showMyProfile',authorize.authorized,(req,res)=>{
    controller.showMyProfile(req,res);
})
router.put('/edit',authorize.authorized,function(req,res){
    controller.edit(req,res);
})
router.delete('/delete',authorize.authorized,(req,res)=>{
    controller.delete(req,res);
})
export default router;