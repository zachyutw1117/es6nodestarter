//routes/auth.js
import express from 'express';
import _ from 'lodash';
import controller from '../controllers/auth.controller';
import User from '../models/user.model';
import authorize from '../services/authorize';

const router = express.Router();

const testController = {}
testController.jwtTokenTest = async function(req,res){ 
    if (req.token) {
        const reqUser = await User.findById(req.token.id);
        const user = User.getMapUser(reqUser);
        res.send({message: "success ", user});
    }else{
        res.send({message: "failed "});
    }
}
router.post('/login', authorize.passportLocal,function(req,res){
    controller.login(req,res);
});
router.post('/jwtTokenTest', authorize.authorizedOptional, function (req, res) {
    testController.jwtTokenTest(req,res)
});

router.post('/signUp' ,authorize.authorizedClientSecret ,function(req,res){

    controller.singUp(req, res);
});

router.post('/sendVerificationCode',authorize.authorizedClientSecret,function(req,res){
    controller.sendVerificationCode(req,res);
});


router.post('/signIn',authorize.passportLocalPhone,function(req,res){
    controller.signIn(req,res);
});

export default router;