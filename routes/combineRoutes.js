import auth from './auth.route';
import user from './user.route';
import cars from './cars.route';



const combineRoutes = (app)=>{
    app.use('/api/user',user);
    app.use('/api/auth', auth);
    app.use('/api/cars', cars);
}

export default combineRoutes;