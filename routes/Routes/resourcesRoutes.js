export default (router,controller)=>{
    router.get('/list', (req, res) => {
        controller.getList(req, res);
    });
    
    router.post('/add', (req, res) => {
        controller.add(req, res);
    });
    router.delete('/remove', (req, res) => {
        controller.delete(req, res);
    });
    router.post('/addlist', (req, res) => {
        controller.addlist(req, res);
    });
    router.get('/list/:id',(req, res) => {
        controller.getOne(req,res);
    })
    router.put('/update/:id',(req,res)=>{
        controller.update(req,res);
    })
    return router;
}