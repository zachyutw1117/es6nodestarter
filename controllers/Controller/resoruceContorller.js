import logger from '../../core/logger/app-logger';
import _ from 'lodash';

import {omitBasic} from '../../models/Model/resourcesModel';



export default(Model, ModalName = "notDefine") => {
    const controller = {};
    const resError={};
    const success={};
    controller.getList = async(req, res) => {
        const name = "getList";
        try {
    
            const datas = await Model.getList();
            const newDatas = datas;
            // console.log(newDatas)
            success.message = `send all ${ModalName}s ${JSON.stringify(datas).slice(0, 200)}...`;
            logger.info(success.message);
            success.datas = newDatas;
            res.send(success);
        } catch (err) {
            logger.error(`Error in getting ${ModalName} ${name}-  + ${err}`);
            resError.message=`Got error in ${ModalName} ${name}`;
            res.status(400).send(resError);
        }
    }

    controller.getOne = async(req, res) => {
        const name = "getOne";
        try {
            // console.log(req.params.id)
            const id = req.params.id || req.query.id;
            console.log(id);
            const data = await Model.getOne(id);
            const newData = omitBasic(data);
            logger.info(`sending ${ModalName}... + ${data}`);
            res.send(newData);

        } catch (err) {
            const resError={};
            logger.error(`Error in getting ${ModalName} ${name}-  + ${err}`);
            resError.message=`Got error in ${ModalName} ${name}`;
            res.status(400).send(resError);
        }
    }

    controller.update = async(req, res) => {
        const name = "update";
        try {
            let data = {}
            data = await Model.update(req.params.id, req.body);
            logger.info(`sending ${ModalName} ${name}`);
            const newData = omitBasic(data);
            res.send(newData);
        } catch (err) {
            console.log(err);
            logger.error(`Error in update ${ModalName} ${name}-  + ${err}`);
            resError.message=`Got error in ${ModalName} ${name}`;
            res.status(400).send(resError);
        }
    }

    controller.add = async(req, res) => {
        const name = "add";
        try {
            let data = Model(req.body);
            const saved = await Model.add(data);
            logger.info(`Adding ${ModalName}... + ${saved}`);
            res.send('added: ' + saved);
        } catch (err) {
            logger.error(`Error in getting ${ModalName} ${name}-  + ${err}`);
            resError.message=`Got error in ${ModalName} ${name}`;
            res.status(400).send(resError);
        }
    }

    controller.delete = async(req, res) => {
        const name = "delete"
        try {
            const {name} = req.body;
            const model = await Model.getOne(name);
            const removed = await Model.delete(name);
            logger.info(`Deleted ${ModalName}- ' + ${removed}`);
            res.send(model);
        } catch (err) {
            logger.error(`Error in getting ${ModalName} ${name}-  + ${err}`);
            resError.message=`Got error in ${ModalName} ${name}`;
            res.status(400).send(resError);
        }
    }

    controller.addlist = async(req, res) => {
        const name = "addlist"
        const {list} = req.body;
        let errors = {}
        list.map(async(item) => {
            try {
                const data = Model(item);
                const saved = await Model.add(data);
                logger.info(`Adding ${ModalName}... + ${saved}`);
            } catch (err) {
                console.log(err);
                errors.err = err;
                logger.error(`Error in getting ${ModalName} ${name}-  + ${err}`);
                resError.message=`Got error in ${ModalName} ${name}`;
                res.status(400).send(resError);
            }
        })
        if (!errors.err) {
            res.send(list);
        }
    }

    
    return controller;
}
