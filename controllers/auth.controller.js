import User from '../models/user.model';
import logger from '../core/logger/app-logger';
import faker from 'faker';
import _ from 'lodash';
const controller ={};
const ModalName = "auth";
controller.login = async (req,res) => {
    const name="Login";
    try{
        const user = User.getMapUser(req.user)||{};
        res.send({message:"success",user,token:req.token,tokenExpire:req.tokenExpire});
    }
    catch(err){
        logger.error(`Error in  ${ModalName} ${name}- ' + err`);
        res.send(`Got error in ${name}`);
    }
}

controller.signIn = async (req,res)=>{
    const name="signIn";
    try{
        const {password} = req.body
        let user = await User.update(req.user._id,{role:1,password});
        
        user = User.mapUser({...req.user,role:1})||{};

        res.send({message:"success",user,token:req.token,tokenExpire:req.tokenExpire});
    }
    catch(err){
        logger.error(`Error in  ${ModalName} ${name}- ' + err`);
        res.send(`Got error in ${name}`);
    }
}

controller.singUp = async (req,res) => { 
    const name = "singUp";
    try {
        let data = User(req.body);
        console.log(req.body);
        console.log(data)
        let saved = await User.add(data);

        saved = User.mapUser(saved);
        logger.info(`Adding ${ModalName}... + ${saved}`);
        res.send({user:saved});
    }
    catch(err) {
        logger.error(`Error in getting ${ModalName} ${name}-  + ${err}`);
        res.send(`Got error in ${ModalName} ${name}`);
    }
}

controller.sendVerificationCode = async (req,res)=>{
    const name="sendVerificationCode";
    try {
        let fakeVerifyCode = faker.random.number({min:1000,max:9999,precision:1});
        fakeVerifyCode = fakeVerifyCode+""
        console.log(fakeVerifyCode,"fakeVerifyCode");
        const user = {username:req.body.phone,phone:req.body.phone,password:fakeVerifyCode,role:0,verifyCode:fakeVerifyCode,nickname:`User${req.body.phone}`};
        
        const exsitUser = await User.findOne({phone:req.body.phone});
        let saved, updated;
        if(exsitUser){
            updated = await User.update( exsitUser._id, {password:fakeVerifyCode,role:0,verifyCode:fakeVerifyCode });
            console.log(updated,"updated");
            logger.info(`Update ${ModalName}... + ${updated}`);
            res.send({message:"verify user",newUser:false,verifyCode:fakeVerifyCode});
        }
        else{
            let data = User(user);
            saved = await User.add(data);
            console.log("saved");
            // saved = User.getMapUser(saved);
            logger.info(`Adding ${ModalName}... + ${saved}`);
            res.send({message:"new user verfiy",newUser:true,verifyCode:fakeVerifyCode});
        }
    }
    catch(err) {
        logger.error(`Error in getting ${ModalName} ${name}-  + ${err}`);
        res.send(`Got error in ${ModalName} ${name} ${err}`);
    }
}
export default controller;