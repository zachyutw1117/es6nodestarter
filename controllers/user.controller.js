import User from '../models/user.model';
import logger from '../core/logger/app-logger';

import _ from 'lodash';
import {mapping} from '../models/Model/resourcesModel';
import resourcesController from './Controller/resoruceContorller';

const ModalName = "user"
//inheritance resourceController with Basic CRUD

// class FortuneClass extends ResourcesClass{}
// const controller = new FortuneClass(Fortune);
const Model = User;
let controller = resourcesController(Model,ModalName);

controller.signUp = async (req,res)=>{
    const name = "signUp";
    try{
        let data = Model(req.body);
        const datas = await Model.add(data);
        logger.info(`add  ${ModalName} ${name}`);
        res.send(datas);
    }
    catch(err){
        logger.error(`Error in getting ${ModalName} ${name}-  + ${err}`);
        res.send(`Got error in ${ModalName} ${name} ${err}`);
    }
}

controller.localAdd = async (req,res) => { 
    const name = "localAdd";
    try {
        let data = Model(req.body);
        let saved = await Model.add(data);
        saved = Model.mapUser(saved);
        logger.info(`Adding ${ModalName}... + ${saved}`);
        res.send({user:saved});
    }
    catch(err) {
        logger.error(`Error in getting ${ModalName} ${name}-  + ${err}`);
        res.send(`Got error in ${ModalName} ${name} ${err}`);
    }
}

controller.showMyProfile = async (req,res)=>{
    const name = "showMyProfile";
    try{
        console.log(req.token,"token");
        const data = await Model.getOne(req.token.id);
        const newData = Model.mapUser(data);
        logger.info(`sending ${ModalName}... ${newData}+ `);
        res.send({user:newData});

    }catch(err){
        logger.error(`Error in getting ${ModalName} ${name}-  + ${err}`);
        res.send(`Got error in ${ModalName} ${name}`);
    }
}
controller.edit = async (req,res)=>{
    const name= "edit";
    try{
        let data ={}
        console.log(req.token.id)
        // data = await Model.findByIdAndUpdate(req.token.id,{..._.omit(req.body,['username','_id','isVerifiedLandlord','isVerifiedTenant'])},{new: true}).then( (data)=>data).catch((err)=> console.log(err,"model update erro"));
        data = await Model.update( req.token.id, {..._.omit(req.body,['username','_id','isVerifiedLandlord','isVerifiedTenant','role'])});
        logger.info(`sending ${ModalName} ${name}`);
        console.log(data);
        data = Model.mapUser(data);
        
        res.send( {user:data} );
    }catch(err){
        console.log(err);
        logger.error(`Error in update ${ModalName} ${name}-  + ${err}`);
        res.send(`Got error in ${ModalName} ${name} `);
    }
}
controller.delete = async (req,res)=>{
    const name="delete"
    try{
        let data = await Model.getOne(req.token.id);
        const removed = await Model.delete(req.token.id);
        logger.info(`Deleted ${ModalName}- ' + ${removed}`);
        data = Model.mapUser(data);
        res.send({message:"user deleted",user:data});
    }
    catch(err) {
        logger.error(`Error in getting ${ModalName} ${name}-  + ${err}`);
        res.send(`Got error in ${ModalName} ${name}`);
    }
}


export default controller;