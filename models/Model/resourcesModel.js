import _ from 'lodash';
import mongoose, { model } from 'mongoose';





export const omit = (omits=['_id'])=>(data)=> {
    let omitData ={}
    if(!_.isPlainObject(data)){
        omitData =  _.omit(data.toObject(),omits);
    }
    else{
        omitData = _.omit(data,omits);
    }
    return {id:data._id,...omitData}
}

export const omitBasic = (data)=>{
   ;
    let omitedData ={};
    if(!_.isPlainObject(data)){
        omitedData =  _.omit(data.toObject(),["_id","__v"]);
    }
    else{
        omitedData = _.omit(data,["_id","__v"]);
    }
    
    return {id:data._id,...omitedData}
}
export const pick =(picks)=> (data)=>{
    let picked = {};
    let newPicks =_.reject(picks,(pick)=>pick==="_id");
    if(_.isEmpty(data)){
        return picked;
    }
    else if(!_.isPlainObject(data) && typeof(data)==="object"){
        try{
            picked =  _.pick(data.toObject(),newPicks );
        }catch(err){
            console.log(data)
        }
    }
    else{
        picked = _.pick(data,newPicks);
    }
    if(_.indexOf(picks,"_id")>=0){
        return {id:data._id,...picked}
    }
    else{
        return picked;
    }
    
}

export const basicParams = ["_id","updatedAt","createdAt"];

export const basicMapped = pick(basicParams);
export const mapping={omit,omitBasic,pick,basicParams,basicMapped};

export default (Model)=>{
    Model.getList = (fields=[]) => Model.find({}).select('title');
    Model.searchList  = (params,fields=[])=>Model.find(params).select(fields.join(' '));
    Model.add = (data) =>  data.save();
    Model.delete = (_id) =>  Model.remove({_id},(err)=>console.log(err));
    Model.getOne = (_id)=> Model.findOne({_id},(err)=>console.log(err));
    Model.addList = (list)=> list.map((data)=>data.save());
    Model.update = (_id,data)=> Model.findByIdAndUpdate(_id,data,{new: true}).then( (data)=>data).catch((err)=> console.log(err,"model update erro"));
    Model.findManyByIds = (propIds)=>{
        const ids = _.map(propIds,(id)=>mongoose.Types.ObjectId(id));
        let datas = Model.find({
            '_id':{
                $in:ids
            }
        } ,(err,doc)=>console.log(doc));
        return datas;
    }
    Model.basicMapped = basicMapped;
    Model.mapping = mapping;
    Model.schema.set('toJSON',{
        transform:function (doc,ret,options){
            ret.id =ret._id;
            delete ret._id;
            delete ret.__v;
        }
    });
    Model.schema.set('toObject',{getters:true});
    return Model
}
