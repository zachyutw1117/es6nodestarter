import mongoose from 'mongoose';
import ResourcesModel,{mapping} from './Model/resourcesModel';
import {emailReg,validateEmail} from './validater';
import {generateHash,validPassword} from '../services/bcryptCode';
import _ from 'lodash';
const {Schema} = mongoose;
const collection = 'User';
const DEFAULT_AVATAR = "https://fakeimg.pl/350x200/?text=Hello";
const DEFAULT_USERNAME = "New User";
const UserSchema = Schema({
        avatar: { type: String,default:DEFAULT_AVATAR},
        nickname: { type: String, default: DEFAULT_USERNAME },
        name: {  familyName: String,  givenName: String,  middleName: String },
        username: { type: String, required: true, unique:true,index: true }, 
        password: { type: String, required: true },
        phone: {  type: String},
        email: { type: String, 
            validate:[validateEmail, 'Please fill a valid email address'], 
            match: [emailReg, 'Please fill a match email address']
        },

}, {collection: collection,timestamps:true});


let Model = mongoose.model(collection, UserSchema);
Model = ResourcesModel(Model);

export default Model;

