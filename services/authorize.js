import passport from 'passport';
import jwt from 'jsonwebtoken';
import _ from 'lodash';
const EXPIRE_MINUTES = 60;
const jwtSecrect = "lasfu";
const clientSecrets = ["1234","4321"];
/* POST login. */
export function authorized(req, res, next) {
    passport.authenticate('jwt', {
        session: false
    }, async(error, token) => {
        console.log(error,"error check");
        if (error || !token ) {
            res.status(401).json({message: 'Unauthorized'});
        }
        try {
            req.token = token
        } catch (error) {
            next(error);
        }
        next();
    })(req, res, next);
}
export function authorizedOptional(req, res, next) {
    passport.authenticate('jwt', {
        session: false
    }, async(err, token) => {
        console.log(err,"error check");
        if (err ) {
            res.status(401).json({message: 'Unauthorized'});
        }
        try {
                req.token = token
        } catch (err) {
            next(err);
        }
        next();
    })(req, res, next);
}

export function authorizedClientSecret(req,res,next){
 
    if(_.findIndex(  clientSecrets, (clientSecret)=>clientSecret ==req.body.clientSecret) >=0){
        console.log("get");
        try {
            next()
        }catch(err){
            console.log('fetch failed', err);
        }
    }
    else{
        const err = new Error('something went wrong oooooooooo');
        return next(err);
    }
}

export function passportLocal(req, res, next) {
    passport.authenticate('local', {
        session: false
    }, (err, user, info) => {
        if (err || !user) {
            return res
                .status(400)
                .json({message: 'Something is not right', user: user});
        }
        req.login(user, {
            session: false
        }, (err) => {
            if (err) {
                // res.send(err);
            }
            try {
            // generate a signed son web token with the contents of user object and return
            // it in the response
                const token = jwt.sign({ id: user._id}, jwtSecrect, {expiresIn: 60 *EXPIRE_MINUTES});
                        req.token = token;
                        req.tokenExpire = EXPIRE_MINUTES + 'mins';
                }  
        catch(err){
            next(err);
        } 
            // return res.json({
            //     user,
            //     token: token,
            //     expire: EXPIRE_MINUTES + 'mins'
            // });
            next();
        });
    })(req, res,next);

}


export function passportLocalPhone(req, res, next) {
    passport.authenticate('local-phone', {
        session: false
    }, (err, user, info) => {
        console.log(err,user)
        if (err || !user) {
            return res
                .status(400)
                .json({message: 'Something is not right', user: user});
        }
        req.login(user, {
            session: false
        }, (err) => {
            if (err) {
                // res.send(err);
            }
            try {
            // generate a signed son web token with the contents of user object and return
            // it in the response
                const token = jwt.sign({ id: user._id,role:user.role}, jwtSecrect, {expiresIn: 60 *EXPIRE_MINUTES});
                        req.token = token;
                        req.tokenExpire = EXPIRE_MINUTES + 'mins';
                }  
        catch(err){
            next(err);
        } 
            // return res.json({
            //     user,
            //     token: token,
            //     expire: EXPIRE_MINUTES + 'mins'
            // });
            next();
        });
    })(req, res,next);

}


const authorize = {
    authorized,
    authorizedOptional,
    authorizedClientSecret,
    passportLocal,
    passportLocalPhone
};
export default authorize;