import passport from 'passport'
import passportJWT from 'passport-jwt';
import LocalStrategy from 'passport-local';
import User from '../models/user.model';

const Passport = () => {
    const JWTStrategy = passportJWT.Strategy;
    const ExtractJWT = passportJWT.ExtractJwt;
    const jwtSecrect = "1234";
    passport.use(new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password'
    }, function (username, password, cb) {
        // this one is typically a DB call. Assume that the returned user object is
        // pre-formatted and ready for storing in JWT
        // console.log(email,password,"step1")
        // console.log(UserModal.findOne({_id:"5bb2aaf6dde15c1e1a1576e0"}) )

        return User
            .findOne({username})
            .then(user => {
                // if (!user || !User.validPassword(password, user.password)) {
                //     return cb(null, false, {message: 'Incorrect email or password.'});
                // }
                if (!user || !( password===user.password)) {
                    return cb(null, false, {message: 'Incorrect email or password.'});
                }
                return cb(null, user.toObject(), {message: 'Logged In Successfully'});
            })
            .catch(err => {
                console.log(err);
                return cb(err);
            });
    }));


    passport.use(new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey: jwtSecrect
    }, (token, done) => {
        console.log(token, "strategy");
        return done(null, token);
    }));

    passport.use("local-phone",new LocalStrategy({
        usernameField: 'phone',
        passwordField: 'verifyCode'
    },function (phone,verifyCode,cb){
        return User.findOne({phone:phone+""}).then(user=>{
            if(!user  ){
                console.log("no user")
                return cb(null,false,{message: "Not valid code"});
            }
            else if(!(verifyCode==user.verifyCode ) ){  
                console.log(user.verifyCode,verifyCode,"not find code");
                console.log("not valid password");
                // User.remove({_id:user._id},(err)=>console.log(err));
                return cb(null,false,{message: "Not valid code"});
            }
            return cb(null, user.toObject(),{message:"Sign In SuccessFully"} );

        })
        .catch(err=>{
            
            return cb(err);
        })

    } ))

    passport.serializeUser((user, done)=> {
        console.log(user)
        done(null, user.id);
    })
    
    passport.deserializeUser((id,done)=>{
        console.log(id)
        
    });
}

export default Passport;
